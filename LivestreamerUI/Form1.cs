﻿using System;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;

namespace LivestreamerUI
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string Arguments = textBox1.Text + " " + comboBox1.SelectedItem.ToString();
            Process.Start(@"C:\Program Files (x86)\Livestreamer\livestreamer.exe", Arguments);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            if (!File.Exists(@"C:\Program Files (x86)\Livestreamer\livestreamer.exe"))
                MessageBox.Show("Livestreamer not found!");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Process.Start(textBox1.Text + "/chat");
        }
    }
}
